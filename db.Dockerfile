# run the ef migration and get the migration script to a .sql file
FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS efbuild
RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs
WORKDIR /src
COPY ["stockApp/stockApp.csproj", "stockApp/"]
RUN dotnet tool install --global dotnet-ef 
ENV PATH="${PATH}:/root/.dotnet/tools"
COPY . .
WORKDIR "/src/stockApp"
RUN dotnet restore
RUN dotnet ef migrations script -o /migration.sql

FROM postgres:latest
COPY --from=efbuild /migration.sql /docker-entrypoint-initdb.d/