﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace stockApp.Models
{
  public class Item
  {
    public int ItemId { get; set;  }
    public string Name { get; set;  } // Name obvs
    public string HomeLocation { get; set; } // Where it is in the house
    public string ShopLocation { get; set; } // Where it is in the shop
    public string Unit { get; set; } // Unit of measurement
    public int ShelfLife { get; set; } // Shelf life...
    public int ParQuantity { get; set; } // Average amount used in a week
    public int CountQuantity { get; set; } // How much there is
    public int CountFrequency { get; set; } // How often to count from last order
    public int OrderQuantity { get; set; } // Amount currently on order
    public DateTime LastCount { get; set; } // Last time it was counted
    public DateTime LastOrder { get; set; } // Last order *collected* (i.e. excl. items on order)
  }
}
