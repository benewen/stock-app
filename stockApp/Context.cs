﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using stockApp.Models;

namespace stockApp
{
  public class Context : DbContext
  {
    public DbSet<Item> Items { get; set; }
    public Context(DbContextOptions<Context> options)
      : base(options){ }
  }
}
