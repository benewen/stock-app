﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using stockApp.Models;

namespace stockApp.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class ItemsController : ControllerBase
  {
    private readonly Context _context;

    public ItemsController(Context context)
    {
      _context = context;
    }

    // GET: api/items
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Item>>> GetItems()
    {
      return await _context.Items.ToListAsync();
    }

    // GET: api/items/byexpiry
    [HttpGet("byexpiry")]
    public async Task<ActionResult<IEnumerable<Item>>> GetItemsByExpiry()
    {
      var res = await _context.Items.ToListAsync();
      Console.WriteLine(res);
      res.Sort((a, b) => a.LastOrder.AddDays(a.ShelfLife).CompareTo(
          b.LastOrder.AddDays(b.ShelfLife)
      ));
      return res;
    }

    // GET: api/items/tocount
    [HttpGet("tocount")]
    public async Task<ActionResult<IEnumerable<Item>>> GetItemsToCount()
    {
      var res = await _context.Items
        .Where(item => item.CountQuantity < item.ParQuantity ||
          item.LastOrder.AddDays(item.CountFrequency) < DateTime.Now
        ).ToListAsync();
      res.Sort((a, b) => a.HomeLocation.CompareTo(b.HomeLocation));
      return res;
    }

    // GET: api/items/toorder
    [HttpGet("toorder")]
    public async Task<ActionResult<IEnumerable<Item>>> GetItemsToOrder()
    {
      var res = await _context.Items
        .Where(item => item.CountQuantity < item.ParQuantity - item.OrderQuantity).ToListAsync();
      res.Sort((a, b) => a.ShopLocation.CompareTo(b.ShopLocation));
      return res;
    }

    // GET: api/items/tocollect
    [HttpGet("tocollect")]
    public async Task<ActionResult<IEnumerable<Item>>> GetItemsToCollect()
    {
      var res = await _context.Items
        .Where(item => item.OrderQuantity > 0
        ).ToListAsync();
      res.Sort((a, b) => a.HomeLocation.CompareTo(b.HomeLocation));
      return res;
    }

    // GET: api/items/:id
    [HttpGet("{id}")]
    public async Task<ActionResult<Item>> GetOrderItems(int id)
    {
      return await _context.Items
        .FirstOrDefaultAsync(item => item.ItemId == id);
    }

    // PUT: api/items/:id
    [HttpPut("{id}")]
    public async Task<ActionResult> PutItem(int id, Item item)
    {
      if (id != item.ItemId)
      {
        return BadRequest();
      }

      _context.Entry(item).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!ItemExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/items
    [HttpPost]
    public async Task<ActionResult<Item>> PostItem(Item item)
    {
      _context.Items.Add(item);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetRoomTask", new { id = item.ItemId }, item);
    }

    // DELETE: api/items
    [HttpDelete("{id}")]
    public async Task<ActionResult<Item>> DeleteItem(int id)
    {
      var roomTask = await _context.Items.FindAsync(id);
      if (roomTask == null)
      {
        return NotFound();
      }

      _context.Items.Remove(roomTask);
      await _context.SaveChangesAsync();

      return roomTask;
    }

        private bool ItemExists(int id)
    {
      return _context.Items.Any(e => e.ItemId == id);
    }
  }
}
