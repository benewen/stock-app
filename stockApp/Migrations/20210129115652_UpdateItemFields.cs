﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace stockApp.Migrations
{
    public partial class UpdateItemFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ParFrequency",
                table: "Items",
                newName: "CountFrequency");

            migrationBuilder.AddColumn<DateTime>(
                name: "LastCount",
                table: "Items",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastCount",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "CountFrequency",
                table: "Items",
                newName: "ParFrequency");
        }
    }
}
