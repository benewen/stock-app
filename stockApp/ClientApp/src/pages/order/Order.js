﻿import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { getItemsByExpiry, getItemsToOrder, putItem } from '../../app/thunks'
import { init } from '../../features/editOrders/editOrdersSlice'

import EditOrders from '../../features/editOrders/EditOrders'

import Modal from 'react-bootstrap/Modal'
import Navbar from 'react-bootstrap/Navbar'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Button from 'react-bootstrap/Button'

class Order extends React.Component {
  constructor(props) {
    super(props)

    this.redirect = this.redirect.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    this.props.getItems(() => { })
  }

  redirect(event) {
    const url = event.target.id
    if (url) {
      this.props.history.push(url)
    }
  }

  handleSubmit(event) {
    const cb = () => {
      this.props.getItems(() => this.redirect(event))
    }

    if (this.props.origin) {
      const entries = Object.entries(this.props.items)
      if (entries.length === 0) cb()

      entries.forEach(([key, val], index) => {
        val.forEach((_item, i) => {

          const item = Object.assign({}, this.props.origin[key][i])
          if (_item.orderQuantity !== '') {
            item.orderQuantity += _item.orderQuantity
          }

          const isLast = index === entries.length - 1 && i === val.length - 1
          if (this.props.origin[key][i].orderQuantity !== item.orderQuantity) {
            this.props.putItem(item, isLast ? cb : () => { })
          }
        })
      })
    }
    cb()
  }


  render() {
    let totalItems = 0
    if (this.props.origin) Object.values(this.props.origin).forEach(val => totalItems += val.length)

    return (
      <Modal show size="lg" centered backdrop="static">
        <Navbar as={Modal.Header} bg="primary" variant="dark">
          <Navbar.Brand>{totalItems > 2 ? `Order - ${totalItems} items to order` : 'Order'}</Navbar.Brand>
        </Navbar>
        <Modal.Body>
          <EditOrders />
        </Modal.Body>

        <Modal.Footer>
          {this.props.origin ?
            <>
              <ButtonGroup>
                <Button variant="outline-secondary" id="/count" onClick={this.redirect}>Recount</Button>
                <Button variant="secondary" id="/" onClick={this.redirect}>Cancel</Button>
              </ButtonGroup>
              <ButtonGroup>
                <Button variant="outline-primary" onClick={this.handleSubmit}>Save</Button>
                <Button variant="primary" id="/" onClick={this.handleSubmit}>Save & Return</Button>
              </ButtonGroup>
            </> : <>
              <ButtonGroup>
                <Button variant="outline-secondary" id="count" onClick={this.redirect}>Count</Button>
                <Button variant="outline-secondary" id="collect" onClick={this.redirect}>Collect</Button>
                <Button variant="secondary" id="/" onClick={this.redirect}>Back</Button>
              </ButtonGroup>
            </>
          }
        </Modal.Footer>
      </Modal>
    )
  }
}

const mapStateToProps = ({ editOrders, getOrders }) => {
  return {
    items: editOrders.items,
    origin: getOrders.items,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    putItem: (item, cb) => dispatch(putItem(item))
      .then(() => cb()),
    getItems: (cb) => dispatch(getItemsByExpiry())
      .then(() => dispatch(getItemsToOrder())
        .then(action => dispatch(init(action.payload)))
      )
      .then(() => cb()),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Order))