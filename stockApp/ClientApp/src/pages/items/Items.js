﻿import React from 'react'
import { connect } from 'react-redux'

import { handleToggle } from '../../features/addItem/addItemSlice'

import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import Navbar from 'react-bootstrap/Navbar'
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Button from 'react-bootstrap/Button'

import GetItems from '../../features/getItems/GetItems'
import AddItem from '../../features/addItem/AddItem'
import EditItems from '../../features/editItems/EditItems'
import GetCounts from '../../features/getCounts/GetCounts'
import GetOrders from '../../features/getOrders/GetOrders'
import GetCollects from '../../features/getCollects/GetCollects'

class Items extends React.Component {

  render() {
    return (
      <>
        <GetCounts />
        <GetOrders />
        <GetCollects />

        <Accordion>
          <Card border="primary" >
            <Navbar as={Card.Header} className="justify-content-between" bg="dark" variant="dark">
              <Navbar.Brand>Items</Navbar.Brand>
              <Form inline>
                <GetItems />
                <Button
                  variant={this.props.isOpen ? "primary" : "outline-primary"}
                  disabled={this.props.isOpen}
                  onClick={() => this.props.handleToggle(!this.props.isOpen)}
                >Add Item</Button>
              </Form>
            </Navbar>
            <AddItem />
          </Card>
          <EditItems />
        </Accordion>
      </>
    )
  }
}

const mapStateToProps = ({ addItem }) => {
  return {
    isOpen: addItem.isOpen,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    handleToggle: (isOpen) => dispatch(handleToggle(isOpen)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Items)