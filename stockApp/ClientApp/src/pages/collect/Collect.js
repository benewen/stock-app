﻿import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { getItemsByExpiry, getItemsToCollect, putItem } from '../../app/thunks'
import { init } from '../../features/editCollects/editCollectsSlice'

import EditCollects from '../../features/editCollects/EditCollects'

import Modal from 'react-bootstrap/Modal'
import Navbar from 'react-bootstrap/Navbar'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Button from 'react-bootstrap/Button'

class Collect extends React.Component {
  constructor(props) {
    super(props)

    this.redirect = this.redirect.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    this.props.getItems(() => { })
  }

  redirect(event) {
    const url = event.target.id
    if (url) {
      this.props.history.push(url)
    }
  }

  handleSubmit(event) {
    const cb = () => {
      this.props.getItems(() => this.redirect(event))
    }

    if (this.props.origin) {
      const entries = Object.entries(this.props.items)
      if (entries.length === 0) cb()

      entries.forEach(([key, val], index) => {
        val.forEach((_item, i) => {

          const item = Object.assign({}, this.props.origin[key][i])

          if (_item.orderQuantity !== '') {
            item.orderQuantity = 0
            item.countQuantity += _item.orderQuantity

            const isLast = index === entries.length - 1 && i === val.length - 1
            this.props.putItem(item, isLast ? cb : () => { })
          }
        })
      })
    }

    cb()
  }

  render() {
    let totalItems = 0
    if (this.props.origin) Object.values(this.props.origin).forEach(val => totalItems += val.length)

    return (
      <Modal show size="lg" centered backdrop="static">
        <Navbar as={Modal.Header} bg="primary" variant="dark">
          <Navbar.Brand>{totalItems > 2 ? `Collect - ${totalItems} items on order` : 'Collect'}</Navbar.Brand>
        </Navbar>
        <Modal.Body>

          <EditCollects />

        </Modal.Body>

        <Modal.Footer>
          {this.props.origin ?
            <>
              <ButtonGroup>
                <Button variant="secondary" id="/" onClick={this.redirect}>Cancel</Button>
              </ButtonGroup>
              <ButtonGroup>
                <Button variant="outline-primary" onClick={this.handleSubmit}>Save</Button>
                <Button variant="primary" id="/" onClick={this.handleSubmit}>Save & Return</Button>
              </ButtonGroup>
            </> : <>
              <ButtonGroup>
                <Button variant="outline-secondary" id="/count" onClick={this.redirect}>Count</Button>
                <Button variant="outline-secondary" id="/order" onClick={this.redirect}>Order</Button>
                <Button variant="secondary" id="/" onClick={this.redirect}>Back</Button>
              </ButtonGroup>
            </>
          }
        </Modal.Footer>
      </Modal>
    )
  }
}

const mapStateToProps = ({ editCollects, getCollects }) => {
  return {
    items: editCollects.items,
    origin: getCollects.items,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    putItem: (item, cb) => dispatch(putItem(item))
      .then(() => dispatch(init))
      .then(() => cb()),
    getItems: (cb) => dispatch(getItemsByExpiry())
      .then(() => dispatch(getItemsToCollect())
        .then(action => dispatch(init(action.payload)))
      )
      .then(() => cb()),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Collect))