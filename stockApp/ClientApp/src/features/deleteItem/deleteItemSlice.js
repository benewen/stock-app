﻿import { createSlice } from '@reduxjs/toolkit'

import { deleteItem } from '../../app/thunks'


const initialState = {
  loading: 'idle',
  error: null,
  isOpen: false,
  item: {},
}

const deleteItemSlice = createSlice({
  name: 'deleteItem',
  initialState,
  reducers: {
    stageDelete: (state, action) => {
      state.item = action.payload
      state.isOpen = true
    },
    unstageDelete: (state, action) => {
      state.isOpen = false
      state.item = {}
    },
  },
  extraReducers: {
    [deleteItem.pending]: (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    },
    [deleteItem.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.item = {}
        state.isOpen = false
        state.currentRequestId = undefined
      }
    },
    [deleteItem.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.error
        state.currentRequestId = undefined
      }
    },
  }
})

export const { stageDelete, unstageDelete } = deleteItemSlice.actions
export default deleteItemSlice.reducer