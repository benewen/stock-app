﻿import React from 'react';
import { connect } from 'react-redux'

import { deleteItem, getItemsByExpiry } from '../../app/thunks'
import { unstageDelete } from './deleteItemSlice'

import Modal from 'react-bootstrap/Modal'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

class DeleteItem extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const item = this.props.item

    return (
      <>
        <Modal
          show={this.props.isOpen}
          onHide={this.props.unstageDelete}
        >
          <Card border="danger">
            <Modal.Header as={Card.Header}>
              <Modal.Title>Danger zone!</Modal.Title>
            </Modal.Header>
            <Modal.Body as={Card.Body}>
              {`Are you sure you want to delete ${item.name}?`}
            </Modal.Body>
            <Modal.Footer as={Card.Footer}>
              <Button variant="secondary" onClick={this.props.unstageDelete}>
                Cancel
              </Button>
              <Button variant="danger" onClick={() => this.props.deleteItem(item.itemId)}>
                Delete
              </Button>
            </Modal.Footer>
          </Card>
        </Modal>
      </>
    )
  }
}

const mapStateToProps = ({ deleteItem }) => {
  return {
    isOpen: deleteItem.isOpen,
    item: deleteItem.item,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    unstageDelete: () => dispatch(unstageDelete()),
    deleteItem: (item) => dispatch(deleteItem(item))
      .then(() => dispatch(getItemsByExpiry())),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeleteItem)