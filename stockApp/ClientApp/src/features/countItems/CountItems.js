﻿import React from 'react'
import { connect } from 'react-redux'

import { init, handleToggle } from './editItemsSlice'

import Accordion from 'react-bootstrap/Accordion'
import EditItem from './EditItem'

class EditItems extends React.Component {
  constructor(props) {
    super(props)

  }

  componentDidUpdate() {
    this.props.init(this.props.items.length)
  }

  render() {
    return (
      <Accordion>
        {this.props.items.map((item, i) =>
          <EditItem key={i} index={i} item={item}/>  
        )}
      </Accordion>
    )
  }
}

const mapStateToProps = ({ getItems, editItems }) => {
  return {
    error: editItems.error,
    items: getItems.items,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    init: (length) => dispatch(init(length)),
    handleToggle: (isOpen) => dispatch(handleToggle(isOpen)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditItems)