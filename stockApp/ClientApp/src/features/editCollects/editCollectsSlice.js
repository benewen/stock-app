﻿import { createSlice } from '@reduxjs/toolkit'

const initialItem = {
  name: '',
  key: '',
  shopLocation: '',
  unit: '',
  shelfLife: '',
  parQuantity: '',
  orderQuantity: '',
  orderFrequency: '',
  orderQuantity: '',
  lastCount: '',
  lastCollect: '',
}

const initialState = {
  loading: 'idle',
  error: null,
  items: {},
}

const editCollectsSlice = createSlice({
  name: 'editCollects',
  initialState,
  reducers: {
    init(state, action) {
      const items = {}
      action.payload.forEach(item => {
        if (items[item.homeLocation]) {
          items[item.homeLocation].push(initialItem)
        } else {
          items[item.homeLocation] = new Array(initialItem)
        }
      })
      state.items = items
    },
    handleChange(state, action) {
      const id = action.payload.id.split('-')
      const valIsNaN = isNaN(action.payload.value)
      state.items[id[0]][id[1]].orderQuantity = valIsNaN ? '' : parseInt(action.payload.value)
    },
  },
})

export const { init, handleChange } = editCollectsSlice.actions

export default editCollectsSlice.reducer