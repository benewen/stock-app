﻿import React from 'react'
import { connect } from 'react-redux'

import { init, handleToggle } from './editItemsSlice'

import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'

import EditItem from './EditItem'
import DeleteItem from '../deleteItem/DeleteItem'


class EditItems extends React.Component {
  constructor(props) {
    super(props)

  }

  componentDidUpdate() {
    this.props.init(this.props.items.length)
  }

  render() {
    return (
      <>
        {
          this.props.items.length > 0 ?
            <>
              {
                this.props.items.map((item, i) =>
                  <EditItem key={i} index={i} item={item} />
                )
              }
            </> :
            <Card border="primary" bg="light">
              <Accordion.Toggle as={Card.Header}>No items</Accordion.Toggle>
            </Card>
        }
        < DeleteItem />
      </>
    )
  }
}

const mapStateToProps = ({ getItems, editItems }) => {
  return {
    error: editItems.error,
    items: getItems.items,
    filter: getItems.filter,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    init: (length) => dispatch(init(length)),
    handleToggle: (isOpen) => dispatch(handleToggle(isOpen)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditItems)