﻿import React from 'react'
import { connect } from 'react-redux'

import { getItemsByExpiry, postItem, putItem } from '../../app/thunks'
import { handleChange, handleToggle } from './editItemsSlice'
import { stageDelete } from '../deleteItem/deleteItemSlice'

import Collapse from 'react-bootstrap/Collapse'
import Accordion from 'react-bootstrap/Accordion'
import ListGroup from 'react-bootstrap/ListGroup'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Badge from 'react-bootstrap/Badge'

class EditItem extends React.Component {
  constructor(props) {
    super(props)

    this.checkIsUnique = this.checkIsUnique.bind(this)
    this.checkIsValid = this.checkIsValid.bind(this)
    this.buildNewItem = this.buildNewItem.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  checkIsUnique() {
    const item = this.props.items[this.props.index]
    return this.props.data.filter(_item => _item.name === item.name).length === 0
  }

  checkIsValid() {
    const item = this.props.items[this.props.index]
    if (item.name !== '') {
      return this.checkIsUnique()
    }
    if (item.countQuantity !== '') {
      return true
    }
    if (item.orderQuantity !== '') {
      return true
    }
    if (item.homeLocation !== '') {
      return true
    }
    if (item.shopLocation !== '') {
      return true
    }
    if (item.unit !== '') {
      return true
    }
    if (item.shelfLife !== '') {
      return true
    }
    if (item.parQuantity !== '') {
      return true
    }
    if (item.countFrequency !== '') {
      return true
    }

    return false
  }

  buildNewItem() {
    const entries = Object.entries(this.props.items[this.props.index])
    const item = { itemId: this.props.item.itemId }

    let isChanged = false

    entries.forEach(([key, val]) => {
      const origin = this.props.item[key]
      if (val === '' || val === origin) {
        item[key] = origin
      } else {
        item[key] = val
        isChanged = true
      }
    })

    return isChanged ? item : undefined
  }

  handleChange(event) {
    const { id, type, value } = event.target
    this.props.handleChange({ id, type, value })
  }

  handleSubmit(event) {
    const type = event.target.id

    let item = this.buildNewItem()

    switch (type) {
      case 'putItem':
        this.props.putItem(item, this.props.index)
        break
      case 'postItem':
        if (!item) {
          item = Object.assign({}, this.props.item)
          if (!this.checkIsValid()) item.name += ' [Copy]'
        }
        delete item.itemId
        this.props.postItem(item, this.props.index)
        break
      case 'deleteItem':
        this.props.stageDelete(this.props.item)
        break
      default:
        break
    }
  }

  render() {
    const index = this.props.index
    const origin = this.props.item
    const item = this.props.items[index]
    const isOpen = this.props.isOpen[index]
    const daysLeft = Math.floor((new Date(origin.lastOrder).getTime() + 86400000 * origin.shelfLife - Date.now()) / 86400000)

    return (
      <Card border='primary'>
          <Accordion.Toggle as={ListGroup.Item} action onClick={() => this.props.handleToggle({ index, isOpen: !isOpen })}>
            {origin ? origin.name : null}{' '}
            {daysLeft < 7 ?
              <Badge pill variant={daysLeft < 0 ? 'danger' : daysLeft <= 1 ? 'warning' : 'info'} >
                {daysLeft < 0 ? `Expired` : daysLeft === 0 ? 'Expires Today' : daysLeft === 1 ? `Expires tomorrow` : `Expires in ${daysLeft} days`}
              </Badge> : null
            }
          </Accordion.Toggle>
        { item ?
          <Collapse in={isOpen}>
            <Card.Body>
              <Form>
                <Form.Group controlId={`${index}-name`}>
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder={origin.name}
                    value={item.name}
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <Form.Group controlId={`${index}-countQuantity`}>
                  <Form.Label>Amount in house</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder={origin.countQuantity}
                    value={item.countQuantity}
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <Form.Group controlId={`${index}-orderQuantity`}>
                  <Form.Label>Amount on order</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder={origin.orderQuantity}
                    value={item.orderQuantity}
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <Form.Group controlId={`${index}-unit`}>
                  <Form.Label>Unit</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder={origin.unit}
                    value={item.unit}
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <Form.Group controlId={`${index}-homeLocation`}>
                  <Form.Label>Home location</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder={origin.homeLocation}
                    value={item.homeLocation}
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <Form.Group controlId={`${index}-shopLocation`}>
                  <Form.Label>Shop location</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder={origin.shopLocation}
                    value={item.shopLocation}
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <Form.Group controlId={`${index}-shelfLife`}>
                  <Form.Label>Shelf life</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder={origin.shelfLife}
                    value={item.shelfLife}
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <Form.Group controlId={`${index}-parQuantity`}>
                  <Form.Label>Par quantity</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder={origin.parQuantity}
                    value={item.parQuantity}
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <Form.Group controlId={`${index}-countFrequency`}>
                  <Form.Label>Count frequency</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder={origin.countFrequency}
                    value={item.countFrequency}
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <br />
                <Button block
                  id="putItem"
                  variant="primary"
                  disabled={!this.checkIsValid()}
                  onClick={this.handleSubmit}
                >Save Item</Button>
                <Button block
                  variant="secondary"
                  onClick={() => this.props.handleToggle({ index, isOpen: false })}
                >Cancel</Button>
                <br />
                <Button block
                  id="postItem"
                  variant="outline-primary"
                  disabled={!this.checkIsUnique()}
                  onClick={this.handleSubmit}
                >{this.checkIsValid() || !this.checkIsUnique() ? 'Save As New Item' : 'Duplicate Item'}</Button>
                <Button block
                  id="deleteItem"
                  variant="outline-danger"
                  onClick={this.handleSubmit}
                >Delete Item</Button>
              </Form>
            </Card.Body>
          </Collapse> : null
        }
      </Card>
    )
  }
}

const mapStateToProps = ({ editItems, getItems }) => {
  return {
    isOpen: editItems.isOpen,
    items: editItems.items,
    data: getItems.data,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    handleChange: (target) => dispatch(handleChange(target)),
    handleToggle: (isOpen) => dispatch(handleToggle(isOpen)),
    putItem: (item, index) => dispatch(putItem(item))
      .then(() => dispatch(handleToggle({ index, isOpen: false })))
      .then(() => dispatch(getItemsByExpiry())),
    postItem: (item, index) => dispatch(postItem(item))
      .then(() => dispatch(handleToggle({ index, isOpen: false })))
      .then(() => dispatch(getItemsByExpiry())),
    stageDelete: (item) => dispatch(stageDelete(item)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditItem)