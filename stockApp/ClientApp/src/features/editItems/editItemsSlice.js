﻿import { createSlice } from '@reduxjs/toolkit'

import { putItem } from '../../app/thunks'

const initialItem = {
  name: '',
  homeLocation: '',
  shopLocation: '',
  unit: '',
  shelfLife: '',
  parQuantity: '',
  countQuantity: '',
  countFrequency: '',
  orderQuantity: '',
  lastCount: '',
  lastOrder: '',
}

const initialState = {
  loading: 'idle',
  error: null,
  items: [],
  isOpen: [],
}

const editItemsSlice = createSlice({
  name: 'editItems',
  initialState,
  reducers: {
    init(state, action) {
      const isOpen = new Array(action.payload)
      const items = new Array(action.payload)

      for (let i = 0; i < action.payload; i++) {
       isOpen[i] = false
       items[i] = initialItem
      }
      state.isOpen = isOpen
      state.items = items
    },
    handleToggle(state, action) {
      state.isOpen[action.payload.index] = action.payload.isOpen
      if (!action.payload.isOpen) {
        state.items[action.payload.index] = initialItem
      }
    },
    handleChange(state, action) {
      const t = action.payload
      const id = t.id.split('-')
      state.items[id[0]][id[1]] = t.type === 'number' ? parseInt(t.value) : t.value
    },
  },
  extraReducers: {
    [putItem.pending]: (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    },

    [putItem.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.currentRequestId = undefined
      }
    },

    [putItem.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.error
        state.currentRequestId = undefined
      }
    },
  }
})

export const { init, handleChange, handleToggle } = editItemsSlice.actions

export default editItemsSlice.reducer