﻿import React from 'react'

import { Link } from 'react-router-dom'

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

class AppBar extends React.Component {
  render() {
    return (
      <Navbar bg="primary" variant="dark">
        <Navbar.Brand as={Link} to='/'>StockApp</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse>
          <Nav>
            <Nav.Link as={Link} to={'/count'}>Count</Nav.Link>
            <Nav.Link as={Link} to={'/order'}>Order</Nav.Link>
            <Nav.Link as={Link} to={'/collect'}>Collect</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default AppBar