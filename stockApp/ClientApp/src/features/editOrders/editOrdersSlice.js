﻿import { createSlice } from '@reduxjs/toolkit'

const initialItem = {
  name: '',
  key: '',
  shopLocation: '',
  unit: '',
  shelfLife: '',
  parQuantity: '',
  orderQuantity: '',
  orderFrequency: '',
  orderQuantity: '',
  lastCount: '',
  lastOrder: '',
}

const initialState = {
  loading: 'idle',
  error: null,
  items: {},
}

const editOrdersSlice = createSlice({
  name: 'editOrders',
  initialState,
  reducers: {
    init(state, action) {
      const items = {}
      action.payload.forEach(item => {
        if (items[item.shopLocation]) {
          items[item.shopLocation].push(initialItem)
        } else {
          items[item.shopLocation] = new Array(initialItem)
        }
      })
      state.items = items
    },
    handleChange(state, action) {
      const id = action.payload.id.split('-')
      const valIsNaN = isNaN(action.payload.value)
      state.items[id[0]][id[1]].orderQuantity = valIsNaN ? '' : parseInt(action.payload.value)
    },
  },
})

export const { init, handleChange } = editOrdersSlice.actions

export default editOrdersSlice.reducer