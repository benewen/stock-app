﻿import React from 'react'
import { connect } from 'react-redux'

import { init } from './editOrdersSlice'

import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'

import EditOrder from './EditOrder'

class EditOrders extends React.Component {
  constructor(props) {
    super(props)

  }

  render() {
    return (
      <Form>
        { this.props.items ?
          <>
            {
              Object.entries(this.props.items).map(([key, val]) =>
                <div key={`${key}`}>
                  <Card bg="light" border="primary">
                    <Card.Header>{key}</Card.Header>
                    <Card.Body>
                      {
                        val.map((item, i) =>
                          <EditOrder item={item} key={`${key}-${i}`} id={`${key}-${i}`} />
                        )
                      }
                    </Card.Body>
                  </Card>
                  <br />
                </div>
              )
            }
          </> :
          <Card bg="light" >
            <Card.Body>No items need ordering!</Card.Body>
          </Card>
        }
      </Form>
    )
  }
}

const mapStateToProps = ({ getOrders, editOrders }) => {
  return {
    error: editOrders.error,
    items: getOrders.items,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    init: (items) => dispatch(init(items)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditOrders)