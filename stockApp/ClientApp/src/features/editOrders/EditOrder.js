﻿import React from 'react'
import { connect } from 'react-redux'

import { handleChange } from './editOrdersSlice'

import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

class EditOrder extends React.Component {
  constructor(props) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    const { id, value } = event.target
    this.props.handleChange({ id, value })
  }

  render() {
    const [shopLocation, index] = this.props.id.split('-')
    const origin = this.props.item
    const arr = this.props.items[shopLocation]
    const item = arr ? arr[index] : undefined
    const recommended = origin.parQuantity - origin.countQuantity - origin.orderQuantity

    return (
      <Form.Group as={Row} controlId={`${shopLocation}-${index}`}>
        <Form.Label column sm={6}>{origin.name}</Form.Label>
        <Col sm={6}>
          <Form.Control
            type="number"
            placeholder={`Recommended: ${recommended}`}
            value={item ? item.orderQuantity : ''}
            onChange={this.handleChange}
          />
        </Col>
      </Form.Group>
    )
  }
}

const mapStateToProps = ({ editOrders }) => {
  return {
    items: editOrders.items,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    handleChange: (target) => dispatch(handleChange(target)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditOrder)