﻿import React from 'react';
import { connect } from 'react-redux'

import { getItemsToCount } from '../../app/thunks';

import { init } from '../editCounts/editCountsSlice';

class GetCounts extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.getItemsToCount()
  }

  render() {
    return (
      <div />
    )
  }
}

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getItemsToCount: () => dispatch(getItemsToCount())
      .then(({ payload }) => dispatch(init(payload))),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GetCounts)