﻿import { createSlice } from '@reduxjs/toolkit'

import { getItemsToCount } from '../../app/thunks'

const initialState = {
  items: {},
  loading: 'idle',
  error: null,
}

const getCountSlice = createSlice({
  name: 'getCount',
  initialState,
  reducers: {

  },
  extraReducers: {
    [getItemsToCount.pending]: (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    },
    [getItemsToCount.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        const items = {}
        action.payload.forEach(item => {
          if (items[item.homeLocation]) {
            items[item.homeLocation].push(item)
          } else {
            items[item.homeLocation] = new Array(item)
          }
        })
        state.items = action.payload.length > 0 ? items : undefined
        state.currentRequestId = undefined
      }
    },
    [getItemsToCount.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.error
        state.currentRequestId = undefined
      }
    },
  }
})

export default getCountSlice.reducer