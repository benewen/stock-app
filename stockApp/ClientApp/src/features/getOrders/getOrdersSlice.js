﻿import { createSlice } from '@reduxjs/toolkit'

import { getItemsToOrder } from '../../app/thunks'

const initialState = {
  items: {},
  loading: 'idle',
  error: null,
}

const getOrderSlice = createSlice({
  name: 'getOrder',
  initialState,
  reducers: {

  },
  extraReducers: {
    [getItemsToOrder.pending]: (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    },
    [getItemsToOrder.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        const items = {}
        action.payload.forEach(item => {
          if (items[item.shopLocation]) {
            items[item.shopLocation].push(item)
          } else {
            items[item.shopLocation] = new Array(item)
          }
        })
        state.items = action.payload.length > 0 ? items : undefined
        state.currentRequestId = undefined
      }
    },
    [getItemsToOrder.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.error
        state.currentRequestId = undefined
      }
    },
  }
})

export default getOrderSlice.reducer