﻿import React from 'react';
import { connect } from 'react-redux'

import { getItemsToOrder } from '../../app/thunks';

import { init } from '../editOrders/editOrdersSlice';

class GetOrders extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.getItemsToOrder()
  }

  render() {
    return (
      <div />
    )
  }
}

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getItemsToOrder: () => dispatch(getItemsToOrder())
      .then(({ payload }) => dispatch(init(payload))),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GetOrders)