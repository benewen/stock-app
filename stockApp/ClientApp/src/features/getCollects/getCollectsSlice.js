﻿import { createSlice } from '@reduxjs/toolkit'

import { getItemsToCollect } from '../../app/thunks'

const initialState = {
  items: {},
  loading: 'idle',
  error: null,
}

const getCollectSlice = createSlice({
  name: 'getCollect',
  initialState,
  reducers: {

  },
  extraReducers: {
    [getItemsToCollect.pending]: (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    },
    [getItemsToCollect.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        const items = {}
        action.payload.forEach(item => {
          if (items[item.homeLocation]) {
            items[item.homeLocation].push(item)
          } else {
            items[item.homeLocation] = new Array(item)
          }
        })
        state.items = action.payload.length > 0 ? items : undefined
        state.currentRequestId = undefined
      }
    },
    [getItemsToCollect.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.error
        state.currentRequestId = undefined
      }
    },
  }
})

export default getCollectSlice.reducer