﻿import React from 'react';
import { connect } from 'react-redux'

import { getItemsToCollect } from '../../app/thunks';

import { init } from '../editCollects/editCollectsSlice';

class GetCollects extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.getItemsToCollect()
  }

  render() {
    return (
      <div />
    )
  }
}

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getItemsToCollect: () => dispatch(getItemsToCollect())
      .then(({ payload }) => dispatch(init(payload))),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GetCollects)