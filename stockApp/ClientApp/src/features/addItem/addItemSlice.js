﻿import { createSlice } from '@reduxjs/toolkit'

import { postItem } from '../../app/thunks'

const initialItem = {
    name: '',
    homeLocation: '',
    shopLocation: '',
    unit: '',
    shelfLife: '',
    parQuantity: '',
    countQuantity: '',
    countFrequency: '',
    orderQuantity: 0,
    lastCount: new Date(Date.now()).toISOString(),
    lastOrder: new Date(Date.now()).toISOString(),
}

const initialState = {
  loading: 'idle',
  error: null,
  isOpen: false,
  item: initialItem,
}

const addItemSlice = createSlice({
  name: 'addItem',
  initialState,
  reducers: {
    handleToggle(state, action) {
      state.isOpen = action.payload
      if (!action.payload) {
        state.item = initialItem
      }
    },
    handleChange(state, action) {
      const t = action.payload
      state.item[t.id] = t.type === 'number' ? parseInt(t.value) : t.value
    },
  },
  extraReducers: {
    [postItem.pending]: (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    },

    [postItem.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.currentRequestId = undefined
      }
    },

    [postItem.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.error
        state.currentRequestId = undefined
      }
    },
  }
})

export const { handleChange, handleToggle } = addItemSlice.actions

export default addItemSlice.reducer