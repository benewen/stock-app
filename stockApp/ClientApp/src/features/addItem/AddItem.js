﻿import React from 'react'
import { connect } from 'react-redux'

import { postItem, getItemsByExpiry } from '../../app/thunks'
import { handleChange, handleToggle } from './addItemSlice'

import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Card from 'react-bootstrap/Card'
import Collapse from 'react-bootstrap/Collapse'
import Form from 'react-bootstrap/Form';

class AddItem extends React.Component {
  constructor(props) {
    super(props)

    this.checkIsUnique = this.checkIsUnique.bind(this)
    this.checkIsValid = this.checkIsValid.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  checkIsUnique() {
    const item = this.props.item
    return this.props.data.filter(_item => _item.name === item.name).length === 0
  }

  checkIsValid() {
    const item = this.props.item

    if (item.name === '') {
      return false
    }
    if (item.countQuantity === '') {
      return false
    }
    if (item.homeLocation === '') {
      return false
    }
    if (item.shopLocation === '') {
      return false
    }
    if (item.unit === '') {
      return false
    }
    if (item.shelfLife === '') {
      return false
    }
    if (item.parQuantity !== '') {
      return true
    }
    if (item.countFrequency === '') {
      return false
    }

    return this.checkIsUnique()
  }

  handleChange(event) {
    const { id, type, value } = event.target
    this.props.handleChange({ id, type, value })
  }

  render() {
    return (
      <Collapse in={this.props.isOpen}>
        <Card.Body>
          <Form>
            <br />

            <Form.Group controlId="name">
              <Form.Control
                type="text"
                placeholder="Name"
                value={this.props.item.name}
                onChange={this.handleChange}
              />
              <Form.Text className="text-muted">
                Item name - must be unique.
                  </Form.Text>
            </Form.Group>

            <Form.Group controlId="countQuantity">
              <Form.Control
                type="number"
                placeholder="Amount in house"
                value={this.props.item.countQuantity}
                onChange={this.handleChange}
              />
              <Form.Text className="text-muted">
                The amount currently held.
                  </Form.Text>
            </Form.Group>

            <Form.Group controlId="unit">
              <Form.Control
                type="text"
                placeholder="Unit"
                value={this.props.item.unit}
                onChange={this.handleChange}
              />
              <Form.Text className="text-muted">
                Unit of measurement - e.g. g, kg, mL, L, ea, etc.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="homeLocation">
              <Form.Control
                type="text"
                placeholder="Home location"
                value={this.props.item.homeLocation}
                onChange={this.handleChange}
              />
              <Form.Text className="text-muted">
                Location at home - e.g. Fridge, Freezer, Pantry, Larder, etc.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="shopLocation">
              <Form.Control
                type="text"
                placeholder="Shop location"
                value={this.props.item.shopLocation}
                onChange={this.handleChange}
              />
              <Form.Text className="text-muted">
                Location in the shop - e.g. Bakery, Fresh Fruit, Drinks, Frozen, etc.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="shelfLife">
              <Form.Control
                type="number"
                placeholder="Shelf life"
                value={this.props.item.shelfLife}
                onChange={this.handleChange}
              />
              <Form.Text className="text-muted">
                The average shelf life in days.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="parQuantity">
              <Form.Control
                type="number"
                placeholder="Par quantity"
                value={this.props.item.parQuantity}
                onChange={this.handleChange}
              />
              <Form.Text className="text-muted">
                The average amount used in a week.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="countFrequency">
              <Form.Control
                type="number"
                placeholder="Count frequency"
                value={this.props.item.countFrequency}
                onChange={this.handleChange}
              />
              <Form.Text className="text-muted">
                The maximum number of days to wait before prompting a count.
              </Form.Text>
            </Form.Group>

            <br />

            <Row>
              <Col>
                <Button block variant="primary"
                  disabled={!this.checkIsValid()}
                  onClick={() => this.props.postItem(this.props.item)}
                >Save Item</Button>
              </Col>
              <Col>
                <Button block variant="secondary"
                  onClick={() => this.props.handleToggle(false)}
                >Cancel</Button>
              </Col>
            </Row>
          </Form>
        </Card.Body>
      </Collapse>
    )
  }
}

const mapStateToProps = ({ getItems, addItem }) => {
  return {
    error: addItem.error,
    isOpen: addItem.isOpen,
    item: addItem.item,
    data: getItems.data,
    filter: getItems.filter,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    handleChange: (target) => dispatch(handleChange(target)),
    handleToggle: (isOpen) => dispatch(handleToggle(isOpen)),
    postItem: (item) => dispatch(postItem(item))
      .then(() => dispatch(handleToggle(false)))
      .then(() => dispatch(getItemsByExpiry())),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddItem)