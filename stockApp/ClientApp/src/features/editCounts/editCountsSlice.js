﻿import { createSlice } from '@reduxjs/toolkit'

const initialItem = {
  name: '',
  key: '',
  shopLocation: '',
  unit: '',
  shelfLife: '',
  parQuantity: '',
  countQuantity: '',
  countFrequency: '',
  orderQuantity: '',
  lastCount: '',
  lastOrder: '',
}

const initialState = {
  loading: 'idle',
  error: null,
  items: {},
}

const editCountsSlice = createSlice({
  name: 'editCounts',
  initialState,
  reducers: {
    init(state, action) {
      const items = {}
      action.payload.forEach(item => {
        if (items[item.homeLocation]) {
          items[item.homeLocation].push(initialItem)
        } else {
          items[item.homeLocation] = new Array(initialItem)
        }
      })
      state.items = items
    },
    handleChange(state, action) {
      const id = action.payload.id.split('-')
      const valIsNaN = isNaN(action.payload.value)
      state.items[id[0]][id[1]].countQuantity = valIsNaN ? '' : parseInt(action.payload.value)
    },
  },
})

export const { init, handleChange } = editCountsSlice.actions

export default editCountsSlice.reducer