﻿import { createSlice } from '@reduxjs/toolkit'

import { getItemsByExpiry } from '../../app/thunks'

const initialState = {
  data: [],
  items: [],
  filter: '',
  loading: 'idle',
  error: null,
}

const getItemsSlice = createSlice({
  name: 'getItems',
  initialState,
  reducers: {
    filter: (state, action) => {
      state.items = state.data.filter(item => item.name.toLowerCase().includes(action.payload.toLowerCase()))
      state.filter = action.payload
    }
  },
  extraReducers: {
    [getItemsByExpiry.pending]: (state, action) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
        state.currentRequestId = action.meta.requestId
      }
    },
    [getItemsByExpiry.fulfilled]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.data = action.payload
        state.items = action.payload.filter(item => item.name.toLowerCase().includes(state.filter.toLowerCase()))
        state.currentRequestId = undefined
      }
    },
    [getItemsByExpiry.rejected]: (state, action) => {
      const { requestId } = action.meta
      if (state.loading === 'pending' && state.currentRequestId === requestId) {
        state.loading = 'idle'
        state.error = action.error
        state.currentRequestId = undefined
      }
    },
  }
})

export const { filter } = getItemsSlice.actions

export default getItemsSlice.reducer