﻿import React from 'react';
import { connect } from 'react-redux'

import { getItemsByExpiry } from '../../app/thunks';
import { filter } from '../getItems/getItemsSlice'

import FormControl from 'react-bootstrap/FormControl';

class GetItems extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.getItemsByExpiry()
  }

  render() {
    return (
      <FormControl
        placeholder="Filter items..."
        onChange={this.props.filter}
        className="mr-sm-2"
      />
    )
  }
}

const mapStateToProps = ({ getItems }) => {
  return {
    isOpen: getItems.isOpen,
    items: getItems.items,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    filter: (e) => dispatch(filter(e.target.value)),
    getItemsByExpiry: () => dispatch(getItemsByExpiry()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GetItems)