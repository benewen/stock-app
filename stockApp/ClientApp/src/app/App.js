import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import AppBar from '../features/appBar/AppBar'
import Items from '../pages/items/Items'
import Count from '../pages/count/Count'
import Order from '../pages/order/Order'
import Collect from '../pages/collect/Collect'

import Container from 'react-bootstrap/Container'

function App() {
  return (
    <Router>
      <AppBar />
      <br />
      <Container>
        <Route path='/' component={Items} />
        <Switch>
          <Route path='/count' component={Count} />
          <Route path='/order' component={Order} />
          <Route path='/collect' component={Collect} />
        </Switch>
      </Container>
      <hr />
    </Router>
  );
}

export default App;
