import { configureStore } from '@reduxjs/toolkit'
import getItemsReducer from '../features/getItems/getItemsSlice'
import addItemReducer from '../features/addItem/addItemSlice'
import editItemsReducer from '../features/editItems/editItemsSlice'
import deleteItemReducer from '../features/deleteItem/deleteItemSlice'
import getCountsReducer from '../features/getCounts/getCountsSlice'
import editCountsReducer from '../features/editCounts/editCountsSlice'
import getOrdersReducer from '../features/getOrders/getOrdersSlice'
import editOrdersReducer from '../features/editOrders/editOrdersSlice'
import getCollectsReducer from '../features/getCollects/getCollectsSlice'
import editCollectsReducer from '../features/editCollects/editCollectsSlice'

export default configureStore({
  reducer: {
    getItems: getItemsReducer,
    addItem: addItemReducer,
    editItems: editItemsReducer,
    deleteItem: deleteItemReducer,
    getCounts: getCountsReducer,
    editCounts: editCountsReducer,
    getOrders: getOrdersReducer,
    editOrders: editOrdersReducer,
    getCollects: getCollectsReducer,
    editCollects: editCollectsReducer,
  },
})
