﻿import { createAsyncThunk } from '@reduxjs/toolkit'

const rootUrl = '/api/items'

export const getItemsByExpiry= createAsyncThunk(
  'items/getByExpiry',
  async () => {
    const url = `${rootUrl}/byexpiry`
    const res = await fetch(url).then(res => res.json())
    return res
  }
)

export const getItemsToCount = createAsyncThunk(
  'items/getToCount',
  async () => {
    const url = `${rootUrl}/tocount`
    const res = await fetch(url).then(res => res.json())
    return res
  }
)

export const getItemsToOrder = createAsyncThunk(
  'items/getToOrder',
  async () => {
    const url = `${rootUrl}/toorder`
    const res = await fetch(url)
      .then(res => res.json())
    return res
  }
)

export const getItemsToCollect = createAsyncThunk(
  'items/getToCollect',
  async () => {
    const url = `${rootUrl}/tocollect`
    const res = await fetch(url)
      .then(res => res.json())
    return res
  }
)

export const postItem = createAsyncThunk(
  'items/postItem',
  async (item) => {
    const url = rootUrl
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item),
    }
    const res = await fetch(url, options)
      .then(res => res.json())
    return res
  }
)

export const putItem = createAsyncThunk(
  'items/putItem',
  async (item) => {
    const url = `${rootUrl}/${item.itemId}`
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item),
    }
  await fetch(url, options)
  }
)

export const deleteItem = createAsyncThunk(
  'items/delete',
  async (id) => {
    const url = `${rootUrl}/${id}`
    const res = await fetch(url, { method: 'DELETE' })
      .then(res => res.json())
    return res
  }
)