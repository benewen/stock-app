#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.
FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs
WORKDIR /src
COPY ["stockApp/stockApp.csproj", "stockApp/"]
RUN dotnet restore "stockApp/stockApp.csproj"
COPY . .
WORKDIR "/src/stockApp"
RUN dotnet build "stockApp.csproj" -c Release -o /app/build
RUN dotnet publish "stockApp.csproj" -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443
RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs
WORKDIR /app
COPY --from=build /app/publish .
ENTRYPOINT ["dotnet", "stockApp.dll"]
